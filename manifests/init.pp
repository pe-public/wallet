# Install packages necessary for the "wallet" resource to work
#
# It depends on
#     wallet-client
#     kerberos, specifically klist/ktutil
#     k5start

class wallet::client {
  include kerberos      
  package { 'wallet-client': ensure => 'present' }

}
