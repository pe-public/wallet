Wallet Module
=============
The module installs `wallet-client` package and provides a `wallet` resource to 
download any type of wallet object and keep a local copy up-to-date.

## Wallet installation

To install wallet client using the module just add

    include wallet

to the manifest. The class installs the necessary prerequisites like wallet-client 
package and kerberos, if it has not been installed already.

## Dependencies

The resource depends on the presence of the following binaries:

- `wallet` from a package `wallet-client`
- `k5start` from a package `kstart`
- MIT or Heimdal kerberos utilities from `krb5-user` or `heimdal-clients` respectively.

## Wallet resource

Wallet resource accepts the following arguments:

#### object (namevar)

A name of an object in wallet. _Required_.

#### ensure

Can be `present` or `absent`, downloading an object from wallet or removing its 
local copy. Optional, defaults to _present_.

#### path

The name of a file for a downloaded wallet object. _Required_. Module supports collecting
multiple keys from different wallet objects in a single keytab. To achieve this,
write multiple wallet resources acquiring keytabs for different principals and point
them to the same local keytab on the disk, providing the same value for the parameter
`path`.

#### type

A type of a wallet object like `file`, `keytab` or `pam-duo`. Optional, defaults 
to _keytab_.

#### auth_principal

A kerberos principal used for authentication to wallet, by default a server's 
host principal. Optional, defaults to a first entry in a keytab.

#### auth_keytab

A keytab file where `auth_principal` keys are stored. Must be an absolute path. 
Optional, defaults to _/etc/krb5.keytab_.

#### owner

A desired owner of a file created out of a wallet object. Can be given as a numeric
_uid_ (like _1001_), string representation of a numeric _uid_ (like _"1001"_) or 
a user name (like _jdoe_). Optional, defaults to not setting an owner. 
Since typically puppet runs as root, that would be a default owner of a file.
#### group

A desired group of a file created out of a wallet object. Can be given as a numeric 
_gid_ (like _1001_), string representation of a numeric _gid_ (like _"1001"_) or 
a group name (like _operator_). Optional, defaults to not setting a group. Since 
typically puppet runs as root, that would be a default group of a file.

#### mode

A desired mode of a file created out of a wallet object. Can be given as a numeric 
_mode_ (like _600_) or a string representation of a numeric _mode_ (like _"600"_). 
Optional, defaults to not setting a mode. Wallet client automatically sets mode 
to 600, which would be a natural default.

#### heimdal

Kerberos distribution, Heimdal if true, otherwise MIT. Optional, defaults to _false_.

#### verify

A boolean enabling or disabling verification of a local copy of a wallet object. 
If verification fails for any reason, it is downloaded from wallet again. 
Optional, defaults to _false_.

With verification _false_ all object types are checked for presence. Keytabs are
additionally checked for the presence of a desired key and keytab integrity. 

With verification set to _true_, keytabs are additionaly checked for validity by
attempting to acquire a kerberos ticket. If it fails, the keytab is downloaded
again. The rest of the objects types are compared with their local copies and if
they are not identical, they are downloaded from wallet again.


## Examples

### Download and maintain a keytab

A keytab wallet object `service/myapplication` is downloaded and stored in a file. 
Host principal in a host keytab is used to authenticate to wallet. Ownership is 
set to allow access by _myapp_ account. Keytab is verified on every puppet run to 
contain the keys of a `service/myapplication` principal. If keys are updated in 
wallet, the local keytab would also be updated with them.

```
wallet { 'service/myapplication':
    type     => 'keytab',
    ensure   => 'present',
    verify   => true,
    mode     => '600',
    owner    => 'myapp',
    group    => 'webconfig',
    path     => '/etc/myapplication/myapp.keytab',
}
```

The same, except a particular key from a host keytab is used to authenticate to 
wallet and keytab is not maintained. It is only checked for a presence of a key 
without worrying about its validity. If keytab is misssing, it would be recreated 
from a wallet item. If a key for a `service/myapplication` principal is missing 
from a keytab, it would be added.

```
wallet { 'service/myapplication':
    type           => 'keytab',
    ensure         => 'present',
    verify         => false,
    auth_principal => 'host/server2.stanford.edu',
    path           => '/etc/myapplication/myapp.keytab',
}
```

### Download and maintain a file

A file with a shibboleth key stored in wallet is downloaded. Host principal found 
in a host keytab is used to authenticate to wallet. The content of a shibboleth 
key is maintained. If it gets updated in wallet, a local copy would get updated 
as well.

```
wallet { 'ssl-keypair/server.stanford.edu/shibboleth':
    type     => 'file',
    ensure   => 'present',
    verify   => true,
    path     => '/etc/shibboleth/sp-key.pem',
}
```

### Get Duo configuration for a server

A duo configuration is downloaded in a local file. Host principal found in a host 
keytab is used to authenticate to wallet. The content of a duo configuration is 
not maintained. If a configuration file gets missing it would be downloaded again, 
but local modifictaions of its content won't be overwritten.

```
wallet { 'server.stanford.edu':
    type     => 'pam-duo',
    ensure   => 'present',
    verify   => false,
    path     => '/etc/security/${hostname}.conf',
}
```

### Delete a local copy of a wallet object

```
wallet { 'ssl-key/server.stanford.edu':
    ensure   => 'absent',
    path     => '/etc/ssl/private/server.stanford.edu.key',
}
```

## References
- [Wallet application](https://www.eyrie.org/~eagle/software/wallet/)
- [Wallet object naming](https://www.eyrie.org/~eagle/software/wallet/naming.html)
- [Wallet client commands](https://www.eyrie.org/~eagle/software/wallet/wallet.html)
