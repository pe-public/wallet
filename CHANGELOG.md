# Changelog

## Release v1.2.0

- Fix a bug with ordering. If user or group for the wallet obect
  are created by a required resource, wallet resource has been
  failing. Resolved by not munging user/group parameter values.

## Release v1.1.0

- Add a parameter *object* to specify the name of the wallet
  object instead of using a resource name for that.

## Release v1.0.0

- Initial release
