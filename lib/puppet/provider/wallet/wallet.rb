Puppet::Type.type(:wallet).provide(:wallet) do
  desc "Wallet support"
  confine :osfamily => [:redhat, :debian]
  commands  :wallet => "/usr/bin/wallet",
            :kdestroy => "/usr/bin/kdestroy",
            :kstart => "/usr/bin/k5start",
            :klist => "/usr/bin/klist",
            :ktutil => "/usr/bin/ktutil"

include Puppet::Util::POSIX
include Puppet::Util::Warnings

require 'puppet/parameter/boolean'
require "digest/md5"
require "etc"

#### does resource exist?
##############################

  def exists?
    if File.file?(@resource[:path])
      exists = true

      # do not go further than that if a file
      # is destined to deletion
      return true if @resource[:ensure] == :absent

      # if file is a keytab, read what's in it
      if @resource[:type].to_s == "keytab"
        begin
          if @resource[:heimdal]
            # Heimdal Kerberos is installed
            princs = ktutil("-k", @resource[:path], "list").split("\n")
          else
            # MIT Kerberos is installed
            princs = klist("-k", @resource[:path]).split("\n")
          end

          # Check if a principal we need is present. If absent,
          # we do not need further verification
          exists = (princs.any? { |s| s.include?(@resource[:object]) })

          if (exists and @resource[:verify])
            begin
              # try to get a ticket with the keytab
              kstart("-q", "-f", @resource[:path], @resource[:object])
              # cleanup the keytab
              kdestroy()
            rescue
              # failed to get a ticket i.e. keytab is stale
              exists = false
            end
          end
        rescue
          # Keytab is damaged, get it out of the way
          # and require a refresh from wallet.
          Puppet.notice("#{@resource.instance_variable_get(:@path)}: keytab '#{@resource[:path]}' is damaged. Renaming to '#{@resource[:path]}.bad'")
          File.rename(@resource[:path], "#{@resource[:path]}.bad")
          exists = false
        end
      else
        if @resource[:verify]
          # checksum the wallet object and compare to a
          # local file
          begin
            if @resource[:auth_principal].nil?
              wallet_obj = kstart("-U", "-q", "-f", @resource[:auth_keytab], "--", "/usr/bin/wallet", "get", @resource[:type], @resource[:object])
            else
              wallet_obj = kstart("-q", "-f", @resource[:auth_keytab], @resource[:auth_principal], "--", "/usr/bin/wallet", "get", @resource[:type], @resource[:object])
            end
          rescue Puppet::ExecutionFailure => detail
            raise Puppet::Error, "Failed to acquire wallet object. #{@resource.class.name} #{@resource.name}: #{detail}", detail.backtrace
          end

          local_md5 = Digest::MD5.file(@resource[:path])
          object_md5 = Digest::MD5.hexdigest(wallet_obj)
          exists = (object_md5.to_s == local_md5.to_s)
        end
      end
    else
      # file doesn't exist
      exists = false
    end
  
    return exists
  end



#### create resource
##############################
  
  def create
    begin
      if @resource[:auth_principal].nil?
        kstart("-U", "-q", "-f", @resource[:auth_keytab], "--", "/usr/bin/wallet", "-f", @resource[:path], "get", @resource[:type], @resource[:object])
      else
        kstart("-q", "-f", @resource[:auth_keytab], @resource[:auth_principal], "--", "/usr/bin/wallet", "-f", @resource[:path], "get", @resource[:type], @resource[:object])
      end
    rescue Puppet::ExecutionFailure => detail
      raise Puppet::Error, "Failed to acquire wallet object. #{@resource.class.name} #{@resource.name}: #{detail}", detail.backtrace
    end

    File.chmod(Integer("0" + @resource[:mode].to_s), @resource[:path]) unless @resource[:mode].nil?
    File.send(:chown, name2uid(@resource[:owner]), name2gid(@resource[:group]), @resource[:path]) unless (@resource[:owner].nil? and @resource[:group].nil?)
  end


  
#### destroy resource
##############################

  def destroy
    File.unlink(@resource[:path])
  end



#### manage properties
##############################

  def owner
    stat = @resource.stat
    return :absent unless stat

    stat.uid
  end

  def owner=(should)
    begin
      File.send(:chown, should, nil, @resource[:path])
    rescue => detail
      raise Puppet::Error, _("Failed to set owner to '%{should}': %{detail}") % { should: should, detail: detail }, detail.backtrace
    end
  end

  def group
    stat = @resource.stat
    return :absent unless stat

    stat.gid
  end

  def group=(should)
    # Set our method appropriately, depending on links.
    begin
      File.send(:chown, nil, should, @resource[:path])
    rescue => detail
      raise Puppet::Error, _("Failed to set group to '%{should}': %{detail}") % { should: should, detail: detail }, detail.backtrace
    end
  end

  def mode
    "%o" % (File.stat(@resource[:path]).mode & 007777)
  end

  def mode=(value)
    File.chmod(Integer("0" + value.to_s), @resource[:path])
  end



#### Auxiliary functions
##############################

  def uid2name(id)
    return id.to_s if id.is_a?(Symbol) or id.is_a?(String)
    return nil if id > Puppet[:maximum_uid].to_i

    begin
      user = Etc.getpwuid(id)
    rescue TypeError, ArgumentError
      return nil
    end

    if user.uid == ""
      return nil
    else
      return user.name
    end
  end

  # Determine if the user is valid, and if so, return the UID
  def name2uid(value)
    Integer(value) rescue uid(value) || false
  end

  def gid2name(id)
    return id.to_s if id.is_a?(Symbol) or id.is_a?(String)
    return nil if id > Puppet[:maximum_uid].to_i

    begin
      group = Etc.getgrgid(id)
    rescue TypeError, ArgumentError
      return nil
    end

    if group.gid == ""
      return nil
    else
      return group.name
    end
  end

  def name2gid(value)
    Integer(value) rescue gid(value) || false
  end


end
