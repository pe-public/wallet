Puppet::Type.newtype(:wallet) do
  @doc = "Get a file from wallet"

  require 'puppet/parameter/boolean'
  require 'etc'


  ensurable do
    desc "Get a file from wallet or remove it"
    defaultvalues
    defaultto(:present)
  end
  
  # require any parent directory be created first
  autorequire(:file) do
    [ File.dirname(self[:path]) ]
  end

  # Autorequire the owner and group of the file.
  {:owner => :owner, :group => :group}.each do |type, property|
    autorequire(type) do
      if @parameters.include?(property)
        # The user/group property automatically converts to IDs
        should = @parameters[property].shouldorig
        next unless should
        val = should[0]
        if val.is_a?(Integer) or val =~ /^\d+$/
          nil
        else
          val
        end
      end
    end
  end

  newparam(:name, namevar: true) do
    desc "Wallet object to download"   
  end

  newparam(:object) do
    desc "Wallet object to download"   
    defaultto { @resource[:name] }
  end

  newparam(:path) do
    desc "The local file to save wallet object to"

    validate do |value|
      unless Puppet::Util.absolute_path?(value)
        fail Puppet::Error, "File paths must be fully qualified, not '#{value}'"
      end
    end
  end

  newparam(:type) do
    desc "Type of wallet object"
    defaultto('keytab')
    newvalues('file', 'keytab', 'duo-pam', 'duo-radius' 'duo-rdp')
  end 

  newparam(:auth_keytab) do
    desc "Keytab used to authenticate to wallet"
    defaultto('/etc/krb5.keytab')

    validate do |value|
      unless Puppet::Util.absolute_path?(value)
        fail Puppet::Error, "File paths must be fully qualified, not '#{value}'"
      end
    end    
  end 

  newparam(:auth_principal) do
    desc "Principal in auth_keytab used to authenticate to wallet"

    validate do |value|
      unless /^(host|service|webauth|smtp|pop|postgres|nfs|lpr|ldap|imap|ftp|cifs|afpserver|HTTP)\/[0-9a-zA-Z\.\-]+$/.match(value)
        raise Puppet::Error, "Principal name #{value} is invalid."
      end
    end    
  end 

  newparam(:heimdal, :boolean => true, :parent => Puppet::Parameter::Boolean) do
    desc "Kerberos distribution"
    defaultto(:false)
    newvalues(:true, :false)
  end

  newparam(:verify, :boolean => true, :parent => Puppet::Parameter::Boolean) do
    desc "Enable/disable wallet object validation"
    defaultto(:false)
    newvalues(:true, :false)
  end

  newproperty(:owner) do
    desc "Owner of the local file"

    def insync?(current)
      # We don't want to validate/munge users until we actually start to
      # evaluate this property, because they might be added during the catalog
      # apply.
      @should.map! do |val|
        uid = provider.name2uid(val)
        if uid
          uid
        elsif provider.resource.noop?
          return false
        else
          raise "Could not find user #{val}"
        end
      end

      return true if @should.include?(current)

      unless Puppet.features.root?
        warnonce "Cannot manage ownership unless running as root"
        return true
      end

      false
    end

    # We want to print names, not numbers
    def is_to_s(currentvalue)
      super(provider.uid2name(currentvalue) || currentvalue)
    end

    def should_to_s(newvalue)
      super(provider.uid2name(newvalue) || newvalue)
    end
  end 

  newproperty(:group) do
    desc "Group permission on the local file"

    validate do |group|
      raise(Puppet::Error, "Invalid group name '#{group.inspect}'") unless group and group != ""
    end

    def insync?(current)
      # We don't want to validate/munge groups until we actually start to
      # evaluate this property, because they might be added during the catalog
      # apply.
      @should.map! do |val|
        gid = provider.name2gid(val)
        if gid
          gid
        elsif provider.resource.noop?
          return false
        else
          raise "Could not find group #{val}"
        end
      end

      @should.include?(current)
    end

    # We want to print names, not numbers
    def is_to_s(currentvalue)
      super(provider.gid2name(currentvalue) || currentvalue)
    end

    def should_to_s(newvalue)
      super(provider.gid2name(newvalue) || newvalue)
    end
  end 

  newproperty(:mode) do
    desc "Manage the file's mode."
    defaultto(600)

    # make sure we always compare modes as integers
    munge do |value|
      case value
        when String
          if value =~ /^[-0-9]+$/
            value = value.to_i
          end
      end

      return value
    end
  end


  def initialize(hash)
    @clients = {}
    super
    @stat = :needs_stat
  end


  def flush
    # We want to make sure we retrieve metadata anew on each transaction.
    @parameters.each do |name, param|
      param.flush if param.respond_to?(:flush)
    end
    @stat = :needs_stat
  end


  def stat
    return @stat unless @stat == :needs_stat

    method = :stat

    @stat = begin
      Puppet::FileSystem.send(method, self[:path])
    rescue Errno::ENOENT
      nil
    rescue Errno::ENOTDIR
      nil
    rescue Errno::EACCES
      warning _("Could not stat; permission denied")
      nil
    rescue Errno::EINVAL
      warning _("Could not stat; invalid pathname")
      nil
    end
  end

end
